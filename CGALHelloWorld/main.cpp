#include <iostream>
#include "SimplePolygon.h"

int main() {
	std::cout << "Creating a program for partitioning an Area !!" << std::endl;
	MUAVAS::SimplePolygon P;
	P.addPoint(Point_2D(0, 0));
	P.addPoint(Point_2D(3, 3));
	P.addPoint(Point_2D(5, 0));
	P.addPoint(Point_2D(5, 6));
	P.addPoint(Point_2D(0, 6));

	P.DividePolygon(2);

	Segment_2D SegCheck(Point_2D(0, 0), Point_2D(2, 0));
	std::cout << "Length :" << SegCheck.squared_length() << std::endl;

	std::cout << "P=" << P << std::endl;
}