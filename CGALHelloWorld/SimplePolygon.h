#pragma once
#include "TypeDefs.h"
#include "Utils.h"
#include <ostream>

namespace MUAVAS {
	class SimplePolygon {
			int numPoints{ 0 };
			std::shared_ptr<Polygon_2D> mPoly;
			struct intersectionPoint {
				bool available{ false };
				Point_2D point;
				Segment_2D side;
			};
			struct ByArea
			{
				bool operator ()(const SimplePolygon& a, const SimplePolygon& b) const
				{
					return a.Area() < b.Area();
				}
			};
		public:
			SimplePolygon();
			SimplePolygon(Polygon_2D* poly);
			bool addPoint(Point_2D& point);
			void DividePolygon(int number);
			Segment_2D aFindBisector(std::shared_ptr<Segment_2D> mSeg1, std::shared_ptr <Segment_2D> mSeg2);
			intersectionPoint IntersectionWithSides(Line_2D divLine,Point_2D aPoint);
			bool checkIfVertex(Segment_2D segment);
			double Area() const;
			std::vector<SimplePolygon> makeSmallerpoly(Segment_2D aPartitionLine);
			friend std::ostream& operator<< (std::ostream& out, const SimplePolygon& c);			
			~SimplePolygon();
	};
}
