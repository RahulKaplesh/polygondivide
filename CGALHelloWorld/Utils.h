#pragma once

template<typename KeyType, typename ValueType>
std::pair<KeyType, ValueType> get_max(const std::multimap<KeyType, ValueType>& x) {
	using pairtype=std::pair<KeyType, ValueType>;
	return *std::max_element(x.begin(), x.end(), [](const pairtype& p1, const pairtype& p2) {
		return p1.first < p2.first;
		});
}

