#include "SimplePolygon.h"

MUAVAS::SimplePolygon::SimplePolygon()
{
	std::cout << "Attempting to declare a SimplePolygon Class in CPP" << std::endl;
	mPoly = std::shared_ptr<Polygon_2D>(new Polygon_2D());
}

MUAVAS::SimplePolygon::SimplePolygon(Polygon_2D* poly)
{
	mPoly = std::shared_ptr<Polygon_2D>(poly);
}

bool MUAVAS::SimplePolygon::addPoint(Point_2D& point)
{
	try {
		mPoly->push_back(point);
		if (numPoints < 2) {			
			numPoints++;
		}
		else {
			if (mPoly->is_simple()) {
				numPoints++;
			}
			else {
				//std::shared_ptr<Polygon_2D>Sample = mPoly;
				//std::shared_ptr<Polygon_2D>mPoly = std::shared_ptr<Polygon_2D>(new Polygon_2D());
				auto it = mPoly->vertices_begin();
				while (it != mPoly->vertices_end()) {
					if (it == --mPoly->vertices_end()) {
						mPoly->erase(it);
						break;
					}					
					it++;
				}				
				std::cout << "Cannot add this point as it does not create a simple Polygon;" << std::endl;
				return false;
			}
		}
		return true;
		
	}
	catch (std::exception ex) {
		std::cout << "Could not add Point to polygon!!" << std::endl;
		return false;
	}
}

void MUAVAS::SimplePolygon::DividePolygon(int number)
{
	std::cout << "Area of the polygon:" << fabs(mPoly->area()) << std::endl;
	double rArea = fabs(mPoly->area()) / number;
	std::cout << "Area of the smallest polygon:" << fabs(mPoly->area()) / number << std::endl;

	auto it = mPoly->edges_begin();
	
	std::multimap<double, Segment_2D> lMap;
	Segment_2D bisector;

	while (it != mPoly->edges_end()) {
		auto it1 = mPoly->edges_begin();
		while (it1 != mPoly->edges_end()) {			
			if (it != it1) {
				bisector = aFindBisector(std::shared_ptr<Segment_2D>(new Segment_2D(*it)), std::shared_ptr<Segment_2D>(new Segment_2D(*it1)));
				double length = std::sqrt(bisector.squared_length());
				lMap.insert(std::pair<double, Segment_2D>(length, bisector));
			}
			++it1;
		}
		it++;
	}
	 // Need to find the longest bisector 
		
	auto maxBisector = get_max(lMap);
	Line_2D mProjectionLine;
	Point_2D mPrjctedPt;
	Line_2D connectingLine;
	intersectionPoint iPoint;
	Segment_2D partitionLine;

	// Fo finding which vertex has spliting line 
	auto vertexit = mPoly->vertices_begin();
	while (vertexit != mPoly->vertices_end()) {
		std::cout << "Evaluation commencing for : " << *vertexit << std::endl;	
		mProjectionLine = Line_2D(maxBisector.second);
		mPrjctedPt = mProjectionLine.projection(*vertexit);
		std::cout << "Projected Point is :" << mPrjctedPt << std::endl;
		if (mProjectionLine.has_on(mPrjctedPt)) {
			connectingLine = mProjectionLine.perpendicular(mPrjctedPt);   // Because of this i guess the oriented line isnt proper will need to check the code in the second iteration
		}
		else {
			connectingLine = Line_2D(*vertexit, mPrjctedPt);
		}	
		std::cout << "Connecting Line is :" << connectingLine << std::endl;

		iPoint = IntersectionWithSides(connectingLine,*vertexit);    // Needs to be implemented 

		if (iPoint.available == false) {
			std::cout << "No Intersection point available" << std::endl;
			vertexit++;
			// Continue to the next vertex 
			continue;
		}
		else {
			std::cout << "Point of Intersection is : " << iPoint.point << std::endl;
			// Need to check if the partition line isnt the edge
			partitionLine = Segment_2D(*vertexit, iPoint.point);
			std::cout << "Partition Lin : " << partitionLine << std::endl;
			if (checkIfVertex(partitionLine)) {
				vertexit++;
				partitionLine = Segment_2D();
				continue;
			}
			else {
				std::cout << "Found a divider" << std::endl;
				break;
			}
		}		
	}

	std::vector<SimplePolygon> smallerPolygons;

	if (partitionLine != Segment_2D()) {
		smallerPolygons = makeSmallerpoly(partitionLine);
		// Always taking the area of the smaller polygon for comparison
		auto smallPoly = std::min_element(smallerPolygons.begin(), smallerPolygons.end(),
			[](const SimplePolygon& a, const SimplePolygon& b)
			{
				return a.Area() < b.Area();
			});
		std::cout << "Smaller Polygons Area : " << smallPoly->mPoly->area() << std::endl;

		// Moving the line
		std::vector<Point_2D> endsofside;
		endsofside.push_back(iPoint.side.source());
		endsofside.push_back(iPoint.side.target());
		//Need to append the polygons and calculate area 

		// Find other areas and reiterate 
		// RK : This needs to be done Anbody can contribute !!
	}
	else {
		//I havent found a partition line need to do divison from edges midpoint // In case of squares and move the proportional to the opposite sides to cut the area
		std::cout << "I havent found a partition line need to do divison from edges midpoint" << std::endl;
		
	}
	
	std::cout << "Chosen Bisector Length : " << maxBisector.first << " & Bisector is : " << maxBisector.second << std::endl;
}

Segment_2D MUAVAS::SimplePolygon::aFindBisector(std::shared_ptr<Segment_2D> mSeg1, std::shared_ptr <Segment_2D> mSeg2)
{
	Point_2D mid_Point1 = CGAL::midpoint(mSeg1->start(), mSeg1->target());
	//std::cout << "MidPoint 1" << mid_Point1 << std::endl;

	Point_2D mid_Point2 = CGAL::midpoint(mSeg2->start(), mSeg2->target());
	//std::cout << "MidPoint 2" << mid_Point2 << std::endl;

	return Segment_2D(mid_Point1, mid_Point2);
}

MUAVAS::SimplePolygon::intersectionPoint MUAVAS::SimplePolygon::IntersectionWithSides(Line_2D divLine, Point_2D aPoint)
{
	MUAVAS::SimplePolygon::intersectionPoint iP;
	auto it = mPoly->edges_begin(); 
	while (it != mPoly->edges_end()) {
		// To calculate and return intersector 
		CGAL::cpp11::result_of<Intersect_2D(Segment_2D,Line_2D)>::type result = intersection(*it, divLine);
		if (result) {
			if (const Point_2D *p = boost::get<Point_2D>(&*result)) {
				if (*p != aPoint){
					iP.available = true;
					iP.point = Point_2D(*p);
					iP.side = Segment_2D(*it);
					return iP;
				}				
			}
		}
		++it;
	}
	iP.available = false;
	iP.point = Point_2D();
	iP.side = Segment_2D();
	return iP;
}

bool MUAVAS::SimplePolygon::checkIfVertex(Segment_2D segment)
{
	std::cout << "Segment is : " << segment << std::endl;
	auto it = mPoly->edges_begin();
	while (it != mPoly->edges_end()) {
		if ((segment.source() == (*it).source() && segment.target() == (*it).target()) || (segment.target() == (*it).source() && segment.source() == (*it).target())) {
			return true;
		}
		it++;
	}
	return false;
}

double MUAVAS::SimplePolygon::Area() const
{
	return std::fabs(mPoly->area());
}

std::vector<MUAVAS::SimplePolygon> MUAVAS::SimplePolygon::makeSmallerpoly(Segment_2D aPartitionLine)
{
	// This method of creating smaller polygons currently not working need to check why ?? It does not create simple polygons Works for this case need to check
	// for other cases!!
	std::vector<Point_2D> onPositiveside;
	std::vector<Point_2D> onNegativeside;

	SimplePolygon posPoly; 
	SimplePolygon negaPoly;

	posPoly.addPoint(Point_2D(aPartitionLine.target()));
	posPoly.addPoint(Point_2D(aPartitionLine.source()));

	auto it = mPoly->vertices_begin();
	while (it != mPoly->vertices_end()) {
		Line_2D check(aPartitionLine);
		if (check.has_on_positive_side(*it)) {
			posPoly.addPoint(Point_2D(*it));
		}
		else if (check.has_on_negative_side(*it)) {
			negaPoly.addPoint(Point_2D(*it));
		}
		++it;
	}
	
	negaPoly.addPoint(Point_2D(aPartitionLine.target()));
	negaPoly.addPoint(Point_2D(aPartitionLine.source()));

	std::vector<SimplePolygon> result;
	result.push_back(posPoly);
	result.push_back(negaPoly);

	return result;
}

MUAVAS::SimplePolygon::~SimplePolygon()
{
	std::cout << "Cleaining Up Polygon" << std::endl;
}

std::ostream& MUAVAS::operator<<(std::ostream& out, const SimplePolygon& c)
{
	// TODO: insert return statement here
	int pointNo{ 0 };
	auto it = c.mPoly->vertices_begin();
	while (it != c.mPoly->vertices_end()) {
		pointNo++;
		Point_2D P(*it);
		out << "Point-" << std::to_string(pointNo) << '\t' << P << '\n';
		it++;
	}
	return out;
}
//