#include <CGAL/Cartesian.h>
#include <CGAL/Partition_traits_2.h>

typedef CGAL::Cartesian<double>								K;
typedef CGAL::Partition_traits_2<K>                         Traits;
typedef Traits::Point_2                                     Point_2D;
typedef Traits::Polygon_2                                   Polygon_2D;
typedef Traits::Segment_2                                   Segment_2D;
typedef Traits::Line_2										Line_2D;
typedef Traits::Intersect_2									Intersect_2D;
//typedef Polygon_2D::Vertex_iterator                         Vertex_iterator;
